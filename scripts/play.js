class Tower extends Array {
    constructor(query, position) {
        super();
        this.DOMref = document.querySelector(query);
        this.position = position;
    }
    isEmpty = () => (this.length === 0);
}

class PuzzlePiece {
    constructor(query, width, canMove = false) {
        this.DOMref = document.querySelector(query);
        this.width = width;
        this.canMove = canMove;
    }
    isGreaterThan = (piece) => this.width > piece.width;
}

/**
 * 
 * DOM - LOGIC:
 * 
 */
const startTower = new Tower("div[data-js='startTower']", "start");
const offsetTower = new Tower("div[data-js='offsetTower']", "offset");
const endTower = new Tower("div[data-js='endTower']", "end");
let pieceOnMove = undefined;

const startGame = () => {

    const piecesContainerEl = startTower.DOMref.firstElementChild;
    const possibilitiesList = [[1, 100], [2, 150], [3, 200], [4, 250]];
    const btnRestartEl = document.querySelector(`button[data-js="btnRestart"]`);
    const alertContainerEl = document.querySelector(`div[data-js="alertContainer"]`);

    for (let i = 0; i < possibilitiesList.length; i++) {

        const order = possibilitiesList[i];
        const divEl = document.createElement('div');

        //<div class="piece piece__1" data-js="piece1"></div>:
        divEl.dataset.js = `piece${order[0]}`;
        divEl.classList.add("piece");
        divEl.classList.add(`piece__${order[0]}`);

        piecesContainerEl.appendChild(divEl);
        startTower.push(new PuzzlePiece(`div[data-js='piece${order[0]}']`, order[1]));
    }

    alertContainerEl.addEventListener("click", () => alertContainerEl.classList.add("hidden"));

    btnRestartEl.addEventListener("click", () => location.reload());

};//startGame()

const getTower = (strTower, strPiece) => {

    if (strTower === `startTower` || strPiece === `startTower`)
        return startTower;

    if (strTower === `offsetTower` || strPiece === `offsetTower`)
        return offsetTower;

    if (strTower === `endTower` || strPiece === `endTower`)
        return endTower;

};// getTower(strTower, strPiece)

const movePiece = evt => {

    const strPieceDataset = evt.target.offsetParent.dataset.js;
    const strTowerDataset = evt.target.dataset.js;
    const tower = getTower(strTowerDataset, strPieceDataset);
    if (pieceOnMove !== undefined) {
        if (tower.isEmpty()) {

            const piecesContainerEl = tower.DOMref.firstElementChild;

            piecesContainerEl.appendChild(pieceOnMove.DOMref);
            tower.push(pieceOnMove);
            pieceOnMove = undefined;
        } else if (pieceOnMove.isGreaterThan(tower[0])) {
            const alertContainerEl = document.querySelector(`div[data-js="alertContainer"]`);
            const alertMsgEl = document.querySelector(`span[data-js="alertMsg"]`);
            const translator = { start: "INÍCIO", offset: "AUXILIAR", end: "FIM" };

            alertMsgEl.innerHTML = `A <strong>peça selecionada</strong> é menor que a primeira paça da torre <strong>${translator[tower.position]}</strong>.`;

            alertContainerEl.classList.remove("hidden");
        } else {
            const piecesContainerEl = tower.DOMref.firstElementChild;

            piecesContainerEl.insertBefore(pieceOnMove.DOMref, piecesContainerEl.firstChild);
            tower.unshift(pieceOnMove);
            pieceOnMove = undefined;
        }
    } else {

        pieceOnMove = tower.shift();
        pieceOnMoveEl.appendChild(pieceOnMove.DOMref);
    }
    resultScreen();
};// movePiece(evt)

const checkResults = () => {
    let result = 0;

    const winner = [];
    winner.push(new PuzzlePiece(`div[data-js='piece1']`, 100));
    winner.push(new PuzzlePiece(`div[data-js='piece2']`, 150));
    winner.push(new PuzzlePiece(`div[data-js='piece3']`, 200));
    winner.push(new PuzzlePiece(`div[data-js='piece4']`, 250));

    endTower.forEach((piece, ind) => {
        if (piece.width === winner[ind].width) {
            result++
        }
    })

    return (result === 4);
};//checkResults()

const resultScreen = () => {
    if (checkResults()) {
        document.querySelector(`div[data-js="resultScreen"]`).classList.remove(`hidden`);
    }
};//resultScreen()

/**
 * 
 * DOM - EVENT LISTENERS:
 * 
 */
const pieceOnMoveEl = document.querySelector('div[data-js="pieceOnMoveContainer"]');
startTower.DOMref.addEventListener(`click`, evt => movePiece(evt));
offsetTower.DOMref.addEventListener(`click`, evt => movePiece(evt));
endTower.DOMref.addEventListener(`click`, evt => movePiece(evt));
startGame();